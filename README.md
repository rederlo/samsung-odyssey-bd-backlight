# Teclado iluminado do Samsung Odyssey no Linux
### 1. Configurando a BIOS do notebook
Esse passo é o mais importante então garanta que está tudo certo.
![alt Samsung Odyssey](https://i.imgur.com/qFr2gyd.png)
 
 Agora instale a sua distribuição favorita, meu exemplo é com o Fedora 33, fique tranquilo(a) essa dica se aplica as outras distribuições, ubunto e etc.

### 2. Confirmar que o módulo samsung::kbd_backlight está presente no sistema
Abra o seu terminal:
```
cd /sys/class/leds
```
 ![alt samsung::kbd_backlight](https://i.imgur.com/L1xVw9Y.png)
 
 Verifique se existe samsung::kbd_backlight no diretório. Se não existir algo deu muito errado revise a 1º parte.
 
 ### 3. O pulo do gato
 Parece bobo, mas sim, não há uma forma simples de utilizar as teclas nativas para o LED funcionar. A combinação FN+F9 não funciona e uma solução para isso é mapear outra combinação. 
 
 A solução é simples mas para facilitar preparei um script para agilizar o processo. 
 Vamos instalar uma dependência brightnessctl.
 
 Fedora
 ```
 sudo dnf install brightnessctl
  ```
Ubuntu
  ```
 sudo apt install brightnessctl
 ```
 
 ```
git clone https://gitlab.com/rederlo/samsung-odyssey-bd-backlight.git

cd samsung-odyssey-bd-backlight 

sudo cp samsung_backlight.sh /bin/samsung_backlight

sudo chmod +x /bin/samsung_backlight
```
 
### 4. Mapear atalhos
Esse exemplo é feito com o gnome-shell.

![alt atalhos personalizados](https://i.imgur.com/XFXUxWW.png)

Como o FN não funciona então substitumos por CTRL.

### 5. Conclusão
 
 ![alt Samsung Odyssey Linux](https://i.imgur.com/yw4OqIz.png)

É uma dica simples mas resolve o problema de não poder utilizar o teclado no linux. Essa dica provavelmente não deve funcionar em notebooks com o sistema já instalado conforme comentamos no início, alguns ajustes são necessários.

E fim! 

![alt Linux Samsung Odyssey](https://i.imgur.com/mTnhdIR.png)

