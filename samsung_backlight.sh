#!/bin/bash

TMP="$(cat "$HOME/.kbd_backlight")"

if [ $TMP -ne 8 ]; then
    TMP=$(($TMP+2)); 
else 
    TMP=0; 
fi

echo "$TMP" > $HOME/.kbd_backlight
brightnessctl --device='samsung::kbd_backlight' set $TMP